# MikTeX Plug&Play
This image contains a full instance of MikTeX and is configured in a way that
you can easily compile your .tex files without having to manually install
dependent packages.


## Content
- [Usage Example: CLI](#cli-example)
- [Usage Example: GitLab CI](#gitlab-ci-example)


- [Scripts & Commands](#scripts--commands)



## Image Tags
- `latest` - The newest MikTeX version from [GitHub MikTeX next](https://github.com/MiKTeX/miktex/tree/next)
- `stable` - The stable MikTeX version from [GitHub MikTeX main](https://github.com/MiKTeX/miktex/tree/main)
- `manual` - A locally compiled MikTeX version for legacy guaranty

## Usage

### CLI Example
```bash
docker run -it \                            # run in an interactive container
-v <SOURCE_FOLDER>:/src  \                  # mount your .tex source directory at /src
-v <MIKTEX_CACHE_FOLDER>:/root/.miktex \    # (optional) mount a cache folder for packages
registry.gitlab.com/theclocktwister/miktex \    # use the default tag (:latest)
    pdflatex -interaction=nonstopmode \     # --- now you can issue your build command ---
    --aux-directory=./ \
    --output-directory=./ \
    YourDocument.tex
```

### GitLab CI Example
```yaml
Compile PDF:
  image: registry.gitlab.com/theclocktwister/miktex:manual

  cache:
    paths:
      -  .miktex
  
    # We want '.miktex' to be our MikTeX local installation directory so that we can make
    # use of the GitLab CI cache to save installed configs and packages for later pipelines.

  before_script:
    - mkdir $(pwd)/.miktex && set_miktex_folder $(pwd)/.miktex

    # We want to override the MikTeX installation folder to a directory inside the repo
    # using 'set_miktex_folder', because GitLab CI forbids access to outer directories.

  script:
    - mpm --update
    - pdflatex -interaction=nonstopmode --output-directory=$(pwd) <YOUR_TEX_FILE>.tex || true

    # You can add the '|| true' to your command to avoid pipeline failure due to MikTeX exiting
    # with code 1, which will be the case for every warning you get. Note: This will also display
    # The job as successful, even if MikTeX failed to compile your output.

  artifacts:
    paths:
      - "*.pdf"

    # At the end, we want GitLab CI to collect any .PDF artifacts that have been created by our
    # build. You may edit this respectively, depending on the file type that you want to produce.
```

## Scripts & Commands
You might want to customize your MikTeX environment to fit your needs.
Therefore, we have a few scripts which can do the job for you:

| Command | Example | Description / Usage |
|-----------|------------------|------------|
| `set_miktex_folder` | `set_miktex_folder $(pwd)/.miktex` | Overrides the MikTeX installation directory. Useful when dealing with GitLab CI wo move the directory into the repo for caching packages


